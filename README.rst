*******************************************************************************
ProvLight: Efficient Workflow Provenance Capture on the Edge-to-Cloud Continuum
*******************************************************************************

`Main publication <https://hal.science/hal-04161546v1>`_

**Scientific Description:** ProvLight is a framework that allows researchers to
efficiently capture provenance data of workflows running on IoT/Edge infrastructures.
ProvLight presents low capture overhead in terms of: capture time; CPU and memory
usage; network usage; and power consumption.

**Functional Description:** ProvLight follows a master/worker architecture where the master
receives the captured data from workers and then translates and sends to provenance systems.
ProvLight also provides a Python library (follows the W3C PROV-DM standard) that allows
users to capture data from their workflows (through application code instrumentation).

Architecture
------------

The architecture of ProvLight follows a **client/server model**:

- **Server**

  - `MQTT-SN broker <https://github.com/eclipse/mosquitto.rsmb.git>`_

- **Client**

  - `ProvLight Python lib <https://gitlab.inria.fr/provlight/provlight>`_ to capture
    provenance data is based on this `MQTT-SN client lib <https://github.com/luanguimaraesla/mqttsn.git>`_

For more details and examples refer to our documentation
--------------------------------------------------------

- `ProvLight design principles and architecture <https://e2clab.gitlabpages.inria.fr/e2clab/provenance/index.html>`_
- `How to capture provenance data of Edge-to-Cloud workflows <https://e2clab.gitlabpages.inria.fr/e2clab/examples/provenance_capture.html>`_





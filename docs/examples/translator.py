_message_client = {
  'id': 19,
  'wf_id': 'synthetic-workload-prov',
  'transformation_id': 'tf4',
  'dependencies': [18],
  'status': 2,
  'time': '2023-01-01 11:42:38',
  'dataset': [
      {
          'id': 'in_1',
          'workflow_id': 'synthetic',
          'derivations': ['out_1'],
          'attributes': {'in': [1]}
      }
   ]
}
import zlib
import time
_message_client = zlib.compress(str(_message_client).encode('ascii'))
print(f"_message_client={_message_client}")
# ######################## SERVER SIDE
import ast
# _message_server = ast.literal_eval(zlib.decompress(_message_client).decode('ascii'))
# simulate multiple messages
multi_msgs = [
    {'id': 18, 'wf_id': 'synthetic-workload-prov', 'transformation_id': 'tf3', 'dependencies': [], 'status': 1, 'time': '2023-01-01 11:42:38', 'dataset': [{'id': 'in_1', 'workflow_id': 'synthetic', 'derivations': ['in_1'], 'attributes': {'in': [1]}}]},
    {'id': 18, 'wf_id': 'synthetic-workload-prov', 'transformation_id': 'tf3', 'dependencies': [], 'status': 2, 'time': '2023-01-01 11:47:22', 'dataset': [{'id': 'out_1', 'workflow_id': 'synthetic', 'derivations': ['out_1'], 'attributes': {'in': [1]}}]},
    {'id': 19, 'wf_id': 'synthetic-workload-prov', 'transformation_id': 'tf4', 'dependencies': [18], 'status': 1, 'time': '2023-01-01 11:48:11', 'dataset': [{'id': 'in_1', 'workflow_id': 'synthetic', 'derivations': ['out_1'], 'attributes': {'in': [1]}}]},
    {'id': 19, 'wf_id': 'synthetic-workload-prov', 'transformation_id': 'tf4', 'dependencies': [18], 'status': 2, 'time': '2023-01-01 11:50:22', 'dataset': [{'id': 'in_1', 'workflow_id': 'synthetic', 'derivations': ['out_1'], 'attributes': {'in': [1]}}]},
]
#
all_tasks = {}
for _message_server in multi_msgs:
    start = time.time()
    if _message_server['status'] == 1:  # Just need to save first message of each task
        all_tasks[_message_server['id']] = _message_server
    # print(f"type={type(_message_server)}")
    # print(f"_message_server={_message_server}")
    dfanalyzer_data = {}
    dependency = {'tags': [], 'ids': []}
    sets = []
    performances = []
    dfanalyzer_data['tag'] = _message_server['transformation_id']
    for dep_task in _message_server['dependencies']:
        # TODO: get 'transformation_id' from previous tasks using 'dependencies'
        # dependency['tags'].append({'tag': 'TODOO'})
        # dependency['tags'].append({'tag': all_tasks[dep_task]['transformation_id']})
        dependency['tags'].append({'tag': all_tasks[dep_task]['transformation_id']})
        dependency['ids'].append({'id': dep_task})
    dfanalyzer_data['dependency'] = dependency
    for ds in _message_server['dataset']:
        sets.append(
            {
                'tag': ds['id'],
                'elements': list(ds['attributes'].values())
            }
        )
    dfanalyzer_data['sets'] = sets
    dfanalyzer_data['status'] = 'RUNNING' if _message_server['status'] == 1 else 'FINISHED'
    dfanalyzer_data['dataflow'] = _message_server['wf_id']
    dfanalyzer_data['transformation'] = _message_server['transformation_id']
    dfanalyzer_data['id'] = _message_server['id']
    if _message_server['status'] == 2:  # 'FINISHED'
        performances.append(
            {
                'startTime': all_tasks[_message_server['id']]['time'],
                # 'startTime': 'TODOO',  # TODO: get 'time' from task 'RUNNING'
                'endTime': _message_server['time']
            }
        )
    dfanalyzer_data['performances'] = performances
    end = time.time()
    print("#" * 80 + f" translation time (sec) = {end - start}")
    # print("_message_client")
    # print(_message_client)
    # print("#" * 20)
    # print("dfanalyzer_data")
    print(dfanalyzer_data)
# #####################################
# {
#   'tag': 'tf4',
#   'dependency': {
#       'tags': [{'tag': 'tf4'}],
#       'ids': [{'id': '18'}]
#   },
#   'sets': [
#       {
#           'tag': 'itf4',
#           'elements': [
#               ['1']
#            ]
#       }
#   ],
#   'status': 'RUNNING',
#   'dataflow': 'synthetic-workload-prov',
#   'transformation': 'tf4',
#   'id': '19',
#   'performances': [
#       {
#           'startTime': '2022-11-04 11:22:36',
#           'endTime': '2022-11-04 11:22:37'
#       }
#   ]
# }

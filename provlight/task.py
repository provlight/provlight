from typing import List
from datetime import datetime
from provlight.workflow import Workflow
from provlight.data import Data
import zlib


class Task:
    id: int
    workflow: Workflow
    transformation_id: str
    dependencies: List[int]
    dataset: List[Data]
    time: str
    status: int

    def __init__(self, _id: int, workflow: Workflow, transformation_id: str,
                 dependencies: List[int]):
        self.id = _id
        self.workflow = workflow
        self.transformation_id = transformation_id
        self.dependencies = dependencies

    def begin(self, dataset: List[Data]):
        self.status = 1
        self.dataset = dataset
        self.time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self._save(self._get_message())

    def end(self, dataset: List[Data]):
        self.status = 2
        self.dataset = dataset
        self.time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        if self.workflow.grouping == 1:
            self._save(self._get_message())
        else:
            self.workflow.tasks.append(self._get_message())
            if len(self.workflow.tasks) == self.workflow.grouping:
                self._save(self.workflow.tasks)
                self.workflow.tasks = []

    def _save(self, _message):
        """
            Publish message to the MQTT-SN broker.
        """
        _message = zlib.compress(str(_message).encode('ascii'))
        self.workflow.context.publish(topic=self.workflow.topic_id, payload=_message, qos=2)

    def _get_message(self):
        _dataset = []
        for ds in self.dataset:
            _dataset.append({
                "id": ds.id,
                "workflow_id": ds.workflow_id,
                "derivations": ds.derivations,
                "attributes": ds.attributes
            })
        _message = {
            "id": self.id,
            "wf_id": self.workflow.id,
            "transformation_id": self.transformation_id,
            "dependencies": self.dependencies,
            "status": self.status,
            "time": self.time,
            "dataset": _dataset
        }
        return _message

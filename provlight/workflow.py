from mqttsn.client import Client, Callback
import os
from datetime import datetime
import time

service_url = os.environ.get('PROVLIGHT_SERVER_URL', "")
service_topic = os.environ.get('PROVLIGHT_SERVER_TOPIC', "")


class WorkflowCallback(Callback):
    def message_arrived(self, topic_name, payload, qos, retained, msgid):
        return True


class Workflow:

    def __init__(self, _id, grouping: int = 1):
        self.id = _id
        self.grouping = grouping
        self.context = None
        self.topic_id = None
        self.tasks = []

    def begin(self):
        client_id = datetime.utcnow().strftime('%H%M%S%f')[:-3]
        self.context = Client(client_id, host=service_url, port=1883)
        self.context.register_callback(WorkflowCallback())
        self.context.connect()  # clean_session=False
        _, self.topic_id = self.context.subscribe(
            topic=f"provlight{service_topic}", qos=2)

    def end(self):
        time.sleep(30)  # wait for ACK messages when using QoS 2
        self.context.disconnect()


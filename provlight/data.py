from typing import List


class Data:
    id: str
    workflow_id: str
    derivations: List[str]
    attributes: dict

    def __init__(self, _id, workflow_id, derivations, attributes):
        self.id = _id
        self.workflow_id = workflow_id
        self.derivations = derivations
        self.attributes = attributes


